// Modules
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

const projectDirectory = path.resolve(__dirname, '..');


const SCSS_LOADER = ExtractTextPlugin.extract({
    fallback: 'vue-style-loader',
    use: [
        {loader: 'css-loader', query: {sourceMap: true}},
        {
            loader: 'sass-loader',
            options: {
                sourceMap: true,
                includePaths: [
                    path.join(projectDirectory, 'node_modules'),
                    path.join(projectDirectory, 'src/style')
                ]
            }
        }
    ]
});


module.exports = function (env/*, argv*/) {

    console.log('ENV', env);

    const isProd = env.isProd;

    const config = {
        // The base directory, an absolute path, for resolving
        // entry points and loaders from configuration.
        context: projectDirectory,
        entry: path.join(projectDirectory, `src/apps/${env.appName}/main.js`),
        output: {
            path: path.join(projectDirectory, 'dist'),
            publicPath: isProd ? '' : '/',
            filename: isProd ? '[name].[hash].js' : '[name].bundle.js',
            chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js'
        },

        module: {
            // See https://github.com/mapbox/mapbox-gl-js/issues/1649
            noParse: /(mapbox-gl)\.js$/,
            rules: [
                {
                    test: /\.scss$/,
                    use: SCSS_LOADER
                },
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            'scss': SCSS_LOADER,
                        }
                        // other vue-loader options go here
                    }
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]?[hash]'
                    }
                }
            ]
        },

        resolve: {
            // List of directories where to search for imported modules
            modules: [
                path.join(projectDirectory, 'src'),
                path.join(projectDirectory, 'node_modules')
            ],
            alias: {
                'vue$': 'vue/dist/vue.esm.js'
            },
            extensions: ['*', '.js', '.vue', '.json']
        },

        devServer: {
            historyApiFallback: true,
            noInfo: true,
            overlay: true,
            contentBase: './src/public',
            disableHostCheck: true,
            // Allow accessing the dev server from any host
            host: '0.0.0.0',
        },

        performance: {
            // Prevent Webpack to complain about modules too big
            hints: false // "warning" or "error"
        },

        devtool: isProd ? '#source-map' : '#eval-source-map',

        plugins: [
            // Only emit files when there are no errors
            new webpack.NoEmitOnErrorsPlugin(),

            // Generate index.html
            new HtmlWebpackPlugin({
                template: path.join(projectDirectory, 'src/public/index.html'),
                inject: 'body'
            }),

            // Extract CSS into its own file
            new ExtractTextPlugin({
                filename: 'css/[name].[contenthash].css',
                disable: !isProd,
                allChunks: true
            })
        ]
    };


    // http://vue-loader.vuejs.org/en/workflow/production.html
    if (isProd) {
        console.log("ENV PROD");

        config.plugins.push(
            // keep module.id stable when vender modules does not change
            new webpack.HashedModuleIdsPlugin(),

            // enable scope hoisting
            new webpack.optimize.ModuleConcatenationPlugin(),

            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,
                compress: {
                    warnings: false
                }
            }),

            // split vendor js into its own file
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                minChunks: function (module, count) {
                    // any required modules inside node_modules are extracted to vendor
                    return (
                        module.resource &&
                        /\.js$/.test(module.resource) &&
                        module.resource.indexOf(path.join(projectDirectory, 'node_modules')) === 0
                    )
                }
            }),

            // extract webpack runtime and module manifest to its own file in order to
            // prevent vendor hash from being updated whenever app bundle is updated
            new webpack.optimize.CommonsChunkPlugin({
                name: 'manifest',
                chunks: ['vendor']
            }),

            // Copy assets from the public folder
            new CopyWebpackPlugin([{
                from: path.join(projectDirectory, 'src/public')
            }])
        );
    } else {
        config.plugins.push(
            new webpack.HotModuleReplacementPlugin()
        )
    }

    return config;
};


