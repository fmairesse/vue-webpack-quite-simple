# vue-webpack-quite-simple

A Webpack setup with hot-reload, CSS extraction and JS chunking. It aims to provide a simpler Webpack setup than the [vue-webpack](https://github.com/vuejs-templates/webpack) template as there is just one configuration file (rather than 10 files divided into 2 folders for vue-webpack). Also, it is designed to easily build multiple applications: the application name is provided as an argument to Webpack.

It is called "quite simple" as opposed to the [webpack-simple](https://github.com/vuejs-templates/webpack-simple) template.

# Structure

```
.
|───build
|  |───webpack.config.js
|  |───... add here other webpack configurations (e.g for tests)
|
|───messages
|  |───... messages in CSV files
|
|───src
|  |
|  |───apps
|  |  |───myapp
|  |  |  |───...every source that is specific to myapp
|  |───core
|  |  |───...put here source for your business logic. No UI here. No VueJS dependency.
|  |───ui
|  |  |───...every shared VueJS file goes here
|  |───utils
|  |  |───...utilities. Do not depend on core nor ui. No VueJS dependency.
|  |
|  |───public
|  |  |───index.html → required. The template for the index.html of each app.
|  |  |───...put here any file you want to be deployed as is. These files are not "import" from your js files and not bundled by webpack.
|  |
|  |───style
|  |  |───common.scss → Rules shared by all applications
|  |  |───myapp.scss  → Imports common.scss. Rules that apply on all myapp views. Other rules should be contained in .vue files.
|
|───test
|  |───unit
|  |  |───...unit tests go here
```

## Adapting the template to your application

### Install Node modules

```yarn install```

Consider committing the ```npm-packages-offline-cache``` directory.

If you have multiple applications, then you should turn off automatic [pruning](https://yarnpkg.com/lang/en/docs/prune-offline-mirror/) of offline packages by editing the [.yarnrc](.yarnrc) file.

### Rename the application

The example application is named "my app". This name should be replaced. Check:

- [package.json](package.json) → `--env.appName=myapp`,
- [src/apps](src/apps) → `myapp` folder,
- [src/style](src/style) → `myapp.scss` file,
- [src/app/main.js](src/apps/myapp/main.js) → reference to `myapp.scss`),


### Edit Webpack configuration

Review and edit [webpack.config.js](build/webpack.config.js). Things you could change:

- [output.path](build/webpack.config.js#L41)
- [resolve.modules](build/webpack.config.js#L83)
- [includePaths](build/webpack.config.js#L19) of sass-loader

### Edit the messages

The messages of the application should be written in one or several CSV files, then exported to JSON with [messages_exporter.py](messages/messages_exporter.py). For instance:
```
cd messages
python messages_exporter.py -i common.csv myapp.csv -o src/public/messages/myapp
``` 
will combine the 2 given CSV files into one JSON file for each language, in the folder `src/public/messages/myapp`.

The CSV files should be formatted as so:

| Key         | en         | fr               |
|-------------|------------|------------------|
|hello_world  |Hello world!|Bonjour le monde !|
|#ignored line|            |                  |

__Note:__ Ideally, we would have a custom Webpack loader that handles directly the CSV file. This was the fastest solution to implement by me.

## Run

### Development server

Messages should have been exported once (`yarn run messages`).

Launch development server: `yarn run dev` then open [http://localhost:8081]([http://localhost:8081]).

No need to refresh the page when you update the code.

### Bundle for production

```
yarn run build
```
bundles your application into a `dist` directory (unless you changed the value of `output.path` in the configuration).


## Links

- [vuejs](https://vuejs.org),
- [vue-i18n](http://kazupon.github.io/vue-i18n): provide the internationalization mechanism,
- [webpack](https://webpack.js.org),
- [yarn](https://yarnpkg.com)