import codecs
import csv
import json
import os


def load_dictionary(path):
	locale_to_dictionary = {}
	with open(path, 'r') as fd:
		reader = csv.reader(fd)
		row = reader.next()
		locales = row[1:len(row)]
		for locale in locales:
			locale_to_dictionary[locale] = dict()
		try:
			while True:
				row = reader.next()
				if len(row) == 0:
					continue
				key = row[0]
				if len(key) == 0 or key[0] == '#':
					continue
				for i, cell in enumerate(row[1:len(row)]):
					dic = locale_to_dictionary[locales[i]]
					text = unicode(cell, "UTF-8")
					dic[key] = text
		except StopIteration:
			pass
	return locale_to_dictionary


def main():
	# Init command line parser
	import argparse
	parser = argparse.ArgumentParser(description="Exports text messages contained in some CVS files into one JSON file.")
	parser.add_argument('-i', '--input', required=True, nargs='+', help='Path of a CSV file')
	parser.add_argument('-o', '--output', required=True, help='Path of the directory containing the JSON files')
	parser.add_argument('-p', '--pretty', help='Pretty JSON formatting', action='store_true')

	# Parse command line
	args = parser.parse_args()

	# Load input files
	inputs = [load_dictionary(p) for p in args.input]

	# Merge inputs
	merged = {}
	for input in inputs:
		for locale, messages in input.iteritems():
			if locale not in merged:
				merged[locale] = {}
			merged[locale].update(messages)

	# Create output directory if needed
	if not os.path.exists(args.output):
		os.makedirs(args.output)

	# Write JSON files
	for locale in merged.keys():
		with codecs.open(os.path.join(args.output, locale + '.json'), 'w', 'utf-8') as f:
			json.dump(merged[locale], f)


if __name__ == "__main__":
	main()
