import Vue from 'vue';
import VueI18n from 'vue-i18n';
import App from './App.vue';
import 'style/myapp.scss';

Vue.use(VueI18n);

// You could get the locale according to the URL or a user preference.
const locale = 'en';

// Load messages asynchronously
import(/* webpackChunkName: "[request]" */ `messages/myapp/${locale}.json`).then(messages => {

    // Initialize VueI18n

    const localeToMessages = {};
    localeToMessages[locale] = messages;

    const i18n = new VueI18n({
        locale,
        fallbackLocale: locale,
        messages: localeToMessages
    });

    // Launch Vue
    new Vue({
        el: '#loading-app',
        render: h => h(App),
        i18n
    });
});
